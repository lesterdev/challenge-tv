package com.lstr.tv.challenge.data.model

import java.io.Serializable

data class PicturesResult(val photos: PicturesPage): Serializable
data class PicturesPage(val page: Int, val pages: Int, val photo: List<Picture>): Serializable
data class Picture(val id: String, val owner: String, val secret: String, val server: String, val farm: String, val title: String): Serializable {
    fun getUrl() = "https://live.staticflickr.com/$server/${id}_${secret}_b.jpg"
}