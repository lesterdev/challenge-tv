package com.lstr.tv.challenge.app.modules

import com.lstr.tv.challenge.data.remote.api.FlickerApi
import com.lstr.tv.challenge.data.repository.FlickerRepositoryImpl
import com.lstr.tv.challenge.domain.usecase.FlickerRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@Suppress("unused")
@InstallIn(SingletonComponent::class)
class DomainModule {

    @Provides
    @Singleton
    fun provideFlickerRepository(api: FlickerApi): FlickerRepository {
        return FlickerRepositoryImpl(api)
    }
}