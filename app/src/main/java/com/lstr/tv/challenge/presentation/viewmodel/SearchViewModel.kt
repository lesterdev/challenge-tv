package com.lstr.tv.challenge.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.lstr.tv.challenge.data.model.Picture
import com.lstr.tv.challenge.domain.FlickerUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(private val flickerUseCase: FlickerUseCase) : ViewModel() {

    private val _searchList = MutableLiveData<List<Picture>>(listOf())
    val searchList: LiveData<List<Picture>> = _searchList

    fun search(query: String) {
        viewModelScope.launch {
            try {
                flickerUseCase.searchPicture(query).collect{
                    _searchList.value = it
                }
            } catch (e: Exception) {
                _searchList.value = listOf()
            }
        }
    }
}