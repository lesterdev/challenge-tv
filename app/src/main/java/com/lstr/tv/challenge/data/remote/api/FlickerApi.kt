package com.lstr.tv.challenge.data.remote.api

import com.lstr.tv.challenge.BuildConfig
import com.lstr.tv.challenge.data.model.PicturesResult
import retrofit2.http.GET
import retrofit2.http.Query

interface FlickerApi {
    @GET("services/rest/")
    suspend fun searchPicture(@Query("method") method: String, @Query("api_key") two:String = BuildConfig.API_KEY, @Query("text") query: String? = null,
    @Query("gallery_id") gallery_id: String = "66911286-72157647277042064", @Query("format") format: String = "json", @Query("nojsoncallback") key: String = "1",
    @Query("safe_search") safe_search: Int = 3,@Query("geo_context") geo_context: Int = 2,
    @Query("page") page: Int = 1, @Query("per_page") per_page: Int = 15): PicturesResult
}