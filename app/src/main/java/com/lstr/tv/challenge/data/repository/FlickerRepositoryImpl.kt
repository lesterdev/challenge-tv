package com.lstr.tv.challenge.data.repository

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.lstr.tv.challenge.data.model.Picture
import com.lstr.tv.challenge.data.remote.ALL
import com.lstr.tv.challenge.data.remote.DEFAULT_PAGE_SIZE
import com.lstr.tv.challenge.data.remote.PicturePagingSource
import com.lstr.tv.challenge.data.remote.SEARCH
import com.lstr.tv.challenge.data.remote.api.FlickerApi
import com.lstr.tv.challenge.domain.usecase.FlickerRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class FlickerRepositoryImpl @Inject constructor(private val apiFlickr: FlickerApi): FlickerRepository {
    private fun getDefaultPageConfig(): PagingConfig {
        return PagingConfig(pageSize = DEFAULT_PAGE_SIZE, enablePlaceholders = true)
    }

    override suspend fun searchPicture(query: String): Flow<List<Picture>> {
        return flow {
            val response = apiFlickr.searchPicture(method = SEARCH, query = query)
            emit(response.photos.photo)
        }
    }

    override fun loadPictureList(): Flow<PagingData<Picture>> {
        return Pager(
            config = getDefaultPageConfig(),
            pagingSourceFactory = {
                PicturePagingSource(apiFlickr)
            }
        ).flow
    }
}
