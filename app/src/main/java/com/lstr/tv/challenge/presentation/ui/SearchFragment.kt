package com.lstr.tv.challenge.presentation.ui

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import androidx.core.app.ActivityOptionsCompat
import androidx.fragment.app.activityViewModels
import androidx.leanback.app.SearchSupportFragment
import androidx.leanback.widget.*
import com.lstr.tv.challenge.data.model.Picture
import com.lstr.tv.challenge.presentation.ui.presenter.PicturePresenter
import com.lstr.tv.challenge.presentation.viewmodel.SearchViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SearchFragment: SearchSupportFragment(), SearchSupportFragment.SearchResultProvider {
    private val viewModel: SearchViewModel by activityViewModels()

    private var mRowsAdapter: ArrayObjectAdapter? = null
    private var mQuery: String? = null

    private val REQUEST_CODE = 121

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mRowsAdapter = ArrayObjectAdapter(ListRowPresenter())
        badgeDrawable = null
        title = ""
        setSearchResultProvider(this)
        setOnItemViewClickedListener(ItemViewClickedListener())
        speechRecognition()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.searchList.observe(viewLifecycleOwner){
            populateAdapter(it ?: listOf())
        }
    }

    private fun populateAdapter(list: List<Picture>) {
        mRowsAdapter!!.clear()

        val title = if (mQuery.isNullOrEmpty()) "" else if(list.isNotEmpty()) "Search results for $mQuery" else "No search results for $mQuery"
        val header = HeaderItem(title)
        mRowsAdapter!!.add(ListRow(header, ArrayObjectAdapter()))
        list.chunked(3).forEach {
            val listRowAdapter = ArrayObjectAdapter(PicturePresenter())
            listRowAdapter.addAll(0, it)
            mRowsAdapter!!.add(ListRow(null, listRowAdapter))
        }
    }

    private fun speechRecognition() {
        if(!hasPermission(requireContext(), Manifest.permission.RECORD_AUDIO)) {
            requestPermissions(arrayOf(Manifest.permission.RECORD_AUDIO), REQUEST_CODE)
        }
    }

    private fun hasPermission(context: Context, permission: String): Boolean {
        return PackageManager.PERMISSION_GRANTED == context.packageManager.checkPermission(
            permission, context.packageName
        )
    }

    override fun getResultsAdapter(): ObjectAdapter? {
        return mRowsAdapter
    }

    override fun onQueryTextChange(newQuery: String): Boolean {
        return true
    }

    override fun onQueryTextSubmit(query: String): Boolean {
        mRowsAdapter!!.clear()
        loadQuery(query)
        return true
    }

    private fun loadQuery(query: String) {
        mQuery = query
        if (!TextUtils.isEmpty(query) && query != "nil") {
            viewModel.search(query)
        }
    }

    private inner class ItemViewClickedListener : OnItemViewClickedListener {
        override fun onItemClicked(itemViewHolder: Presenter.ViewHolder, item: Any?, rowViewHolder: RowPresenter.ViewHolder?, row: Row?) {
            if (item is Picture) {
                val intent = Intent(requireActivity(), DetailActivity::class.java)
                intent.putExtra(DetailActivity.DATA, item)
                val iv = (itemViewHolder.view as ImageCardView).mainImageView
                val bundle = ActivityOptionsCompat.makeSceneTransitionAnimation(requireActivity(), iv, DetailActivity.SHARED_ELEMENT_NAME).toBundle()
                startActivity(intent, bundle)
            }
        }
    }
}
