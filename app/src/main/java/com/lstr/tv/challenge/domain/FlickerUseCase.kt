package com.lstr.tv.challenge.domain

import androidx.paging.PagingData
import com.lstr.tv.challenge.data.model.Picture
import com.lstr.tv.challenge.domain.usecase.FlickerRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class FlickerUseCase @Inject constructor(private val repository: FlickerRepository) {

    suspend fun searchPicture(query: String): Flow<List<Picture>> {
        return repository.searchPicture(query)
    }

    fun loadPictures(): Flow<PagingData<Picture>> {
        return repository.loadPictureList()
    }
}
