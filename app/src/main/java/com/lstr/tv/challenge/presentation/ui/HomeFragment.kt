package com.lstr.tv.challenge.presentation.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.app.ActivityOptionsCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import androidx.leanback.app.VerticalGridSupportFragment
import androidx.leanback.paging.PagingDataAdapter
import androidx.leanback.widget.*
import androidx.lifecycle.lifecycleScope
import androidx.paging.PagingData
import com.lstr.tv.challenge.R
import com.lstr.tv.challenge.data.model.Picture
import com.lstr.tv.challenge.presentation.ui.presenter.PictureDiffCallBack
import com.lstr.tv.challenge.presentation.ui.presenter.PicturePresenter
import com.lstr.tv.challenge.presentation.viewmodel.HomeViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class HomeFragment: VerticalGridSupportFragment() {
    private val viewModel: HomeViewModel by activityViewModels()
    private var pageAdapter = PagingDataAdapter(PicturePresenter(), PictureDiffCallBack())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupUIElements()
        setupEventListeners()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.post {
            val mDefaultBackground = ContextCompat.getDrawable(requireActivity(), R.drawable.default_background)
            view.background = mDefaultBackground
        }
        viewModel.pictureList.observe(viewLifecycleOwner){
            populateAdapter(it)
        }
    }

    private fun setupUIElements() {
        title = getString(R.string.trending_now_flicker)

        val gridPresenter = VerticalGridPresenter()
        gridPresenter.numberOfColumns = 3
        setGridPresenter(gridPresenter)

        adapter = pageAdapter
    }

    private fun setupEventListeners() {
        onItemViewClickedListener = ItemViewClickedListener()
        setOnSearchClickedListener {
            val intent = Intent(requireActivity(), SearchActivity::class.java)
            startActivity(intent)
        }
    }

    private inner class ItemViewClickedListener : OnItemViewClickedListener {
        override fun onItemClicked(itemViewHolder: Presenter.ViewHolder, item: Any, rowViewHolder: RowPresenter.ViewHolder?, row: Row?) {
            if (item is Picture) {
                val intent = Intent(requireActivity(), DetailActivity::class.java)
                intent.putExtra(DetailActivity.DATA, item)
                val iv = (itemViewHolder.view as ImageCardView).mainImageView
                val bundle = ActivityOptionsCompat.makeSceneTransitionAnimation(requireActivity(), iv,
                    DetailActivity.SHARED_ELEMENT_NAME
                ).toBundle()
                startActivity(intent, bundle)
            }
        }
    }

    private fun populateAdapter(data: PagingData<Picture>) {
        lifecycleScope.launch {
            pageAdapter.submitData(data)
        }
    }
}
