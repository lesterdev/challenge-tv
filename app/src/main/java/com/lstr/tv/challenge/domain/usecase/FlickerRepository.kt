package com.lstr.tv.challenge.domain.usecase

import androidx.paging.PagingData
import com.lstr.tv.challenge.data.model.Picture
import kotlinx.coroutines.flow.Flow

interface FlickerRepository {
    suspend fun searchPicture(query: String): Flow<List<Picture>>
    fun loadPictureList(): Flow<PagingData<Picture>>
}