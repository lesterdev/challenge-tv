package com.lstr.tv.challenge.presentation.viewmodel

import androidx.lifecycle.*
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.lstr.tv.challenge.data.model.Picture
import com.lstr.tv.challenge.domain.FlickerUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(private val flickerUseCase: FlickerUseCase) : ViewModel() {

    private val _viewState  = MutableLiveData<HomeUIState>()
    val viewState: LiveData<HomeUIState> = _viewState

    val pictureList: LiveData<PagingData<Picture>> by lazy {
        flickerUseCase.loadPictures().asLiveData().cachedIn(viewModelScope)
    }
}

sealed class HomeUIState {
    object LoadingData : HomeUIState()
    object DataResult : HomeUIState()
    data class Failed(val e: Exception) : HomeUIState()
}