package com.lstr.tv.challenge.data.remote

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.lstr.tv.challenge.data.model.Picture
import com.lstr.tv.challenge.data.remote.api.FlickerApi

class PicturePagingSource(private val apiService: FlickerApi): PagingSource<Int, Picture>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Picture> {
        return try {
            val position = params.key ?: 1
            val response = apiService.searchPicture(method = ALL, page = position, per_page = DEFAULT_PAGE_SIZE)
            val pictureList = response.photos.photo
            val prevKey = if (position == 1) {
                null
            } else {
                position - 1
            }
            val nextKey = if (pictureList.isEmpty()) {
                null
            } else {
                position + 1
            }
            LoadResult.Page(data = pictureList, prevKey = prevKey, nextKey = nextKey)
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, Picture>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(anchorPosition)?.nextKey?.minus(1)
        }
    }
}

const val ALL = "flickr.photos.getRecent"
const val SEARCH = "flickr.photos.search"
const val DEFAULT_PAGE_SIZE = 30
