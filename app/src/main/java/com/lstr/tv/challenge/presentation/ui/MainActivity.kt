package com.lstr.tv.challenge.presentation.ui

import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ProgressBar
import androidx.activity.viewModels
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.lstr.tv.challenge.R
import com.lstr.tv.challenge.presentation.viewmodel.HomeUIState
import com.lstr.tv.challenge.presentation.viewmodel.HomeViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : FragmentActivity() {
    private val viewModel: HomeViewModel by viewModels()

    private lateinit var mErrorFragment: ErrorFragment
    private lateinit var mSpinnerFragment: SpinnerFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.main_browse_fragment, HomeFragment())
                .commitNow()
        }
        viewModel.viewState.observe(this){
            renderState(it)
        }
    }

    private fun renderState(state: HomeUIState) {
        when(state){
            is HomeUIState.LoadingData -> showLoading()
            is HomeUIState.DataResult -> hideLoading()
            is HomeUIState.Failed -> {
                hideLoading()
                showError()
            }
            else -> {}
        }
    }

    private fun showLoading(){
        mSpinnerFragment = SpinnerFragment()
        supportFragmentManager
            .beginTransaction()
            .add(R.id.main_browse_fragment, mSpinnerFragment)
            .commit()
    }

    private fun hideLoading(){
        if (!this::mSpinnerFragment.isInitialized || supportFragmentManager.fragments.find { it == mSpinnerFragment } == null) return
        supportFragmentManager
            .beginTransaction()
            .remove(mSpinnerFragment)
            .commit()
    }

    private fun showError() {
        mErrorFragment = ErrorFragment()
        supportFragmentManager
            .beginTransaction()
            .add(R.id.main_browse_fragment, mErrorFragment)
            .commit()
    }

    class SpinnerFragment : Fragment() {
        override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val progressBar = ProgressBar(container?.context)
            if (container is FrameLayout) {
                val layoutParams =
                    FrameLayout.LayoutParams(SPINNER_WIDTH, SPINNER_HEIGHT, Gravity.CENTER)
                progressBar.layoutParams = layoutParams
            }
            return progressBar
        }
    }

    companion object {
        private val SPINNER_WIDTH = 100
        private val SPINNER_HEIGHT = 100
    }
}