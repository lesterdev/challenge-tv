package com.lstr.tv.challenge.presentation.ui

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.DisplayMetrics
import androidx.leanback.app.BackgroundManager
import androidx.leanback.app.DetailsSupportFragment
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.lstr.tv.challenge.R
import com.lstr.tv.challenge.data.model.Picture

class ImageDetailFragment : DetailsSupportFragment() {

    private var mSelectedPicture: Picture? = null

    private lateinit var mBackgroundManager: BackgroundManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBackgroundManager = BackgroundManager.getInstance(activity)
        mBackgroundManager.attach(requireActivity().window)

        mSelectedPicture = requireActivity().intent.getSerializableExtra(DetailActivity.DATA) as Picture
        if (mSelectedPicture != null) {
            initializeBackground(mSelectedPicture)
        }
    }

    private fun initializeBackground(picture: Picture?) {
        val activity = requireActivity() as? DetailActivity ?: return
        val displayMetrics = DisplayMetrics()
        activity.windowManager.defaultDisplay.getMetrics(displayMetrics)
        val height = displayMetrics.heightPixels
        val width = displayMetrics.widthPixels

        Glide.with(activity)
            .load(picture?.getUrl())
            .apply(RequestOptions().override(width, height))
             .centerCrop().placeholder(R.drawable.loader)
            .diskCacheStrategy(DiskCacheStrategy.DATA)
            .error(R.drawable.default_background)
            .into<SimpleTarget<Drawable>>(object : SimpleTarget<Drawable>() {
                override fun onResourceReady(
                    drawable: Drawable,
                    transition: Transition<in Drawable>?
                ) {
                    mBackgroundManager.drawable = drawable
                }
            })
    }
}