package com.lstr.tv.challenge.presentation.ui

import android.os.Bundle
import android.view.KeyEvent
import androidx.fragment.app.FragmentActivity
import com.lstr.tv.challenge.R

class DetailActivity : FragmentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.details_fragment, ImageDetailFragment())
                .commitNow()
        }
    }
    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        when(keyCode){
            KeyEvent.KEYCODE_DPAD_UP -> { println("$LOG: UP") }
            KeyEvent.KEYCODE_DPAD_DOWN -> { println("$LOG: DOWN") }
            KeyEvent.KEYCODE_DPAD_LEFT -> { println("$LOG: LLL") }
            KeyEvent.KEYCODE_DPAD_RIGHT -> { println("$LOG: RRR") }
            KeyEvent.KEYCODE_BACK -> { println("$LOG: RRR") }
        }
        return super.onKeyDown(keyCode, event)
    }

    companion object {
        const val SHARED_ELEMENT_NAME = "hero"
        const val DATA = "Data"
        const val LOG = "Log"
    }
}