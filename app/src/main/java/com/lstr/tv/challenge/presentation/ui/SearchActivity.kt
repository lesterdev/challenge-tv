package com.lstr.tv.challenge.presentation.ui

import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import com.lstr.tv.challenge.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SearchActivity : FragmentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.details_fragment, SearchFragment())
                .commitNow()
        }
    }
}